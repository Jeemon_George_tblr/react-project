const userController = require('../Controller/userControler');
const router = require('express').Router();
const multer = require('../Helper/multer');
const verifyToken = require('../Helper/tokenAuthentication');

router.post('', userController.addUser);
router.get('',verifyToken, userController.getUsers);
router.post('/login', userController.login);
router.get('/:id',verifyToken,userController.getUserById);
router.put('/:id',verifyToken, userController.updateUser);
router.delete('/:id',verifyToken, userController.deleteUser);
router.post('/img',verifyToken,multer, userController.imgUpload);
router.post('/Googlelogin', userController.googleLogin);
router.post('/Facebooklogin', userController.facebookLogin);



module.exports = router