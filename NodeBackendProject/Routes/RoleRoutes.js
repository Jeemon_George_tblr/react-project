const rolesController = require('../Controller/rollController.js');
const router = require('express').Router();

router.post('/addroll', rolesController.addRoles);
router.get('/getroles', rolesController.getRoles);


module.exports = router