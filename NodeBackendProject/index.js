const express = require('express');
const cors = require('cors');
const path = require('path');

// --------------Running Port ----------------

const PORT = process.env.PORT || 8080

// -------------------- Middleware of the project  -----------------
const app = express();
app.use(cors("http://localhost:3000"));
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));


// -------------------routes ------------------

const RoleRouter = require('./Routes/RoleRoutes.js')
app.use('/api/roles', RoleRouter)


const RoleUser = require('./Routes/UserRoute')
app.use('/api/users', RoleUser)

// -------------------- Server ---------------------

app.listen(PORT, () => {
    console.log(`server is running port is ${PORT}`);
}) 