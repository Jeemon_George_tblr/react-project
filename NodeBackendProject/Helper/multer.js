const multer = require('multer');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/'); // Save files in the 'uploads' folder
    },
    filename: (req, file, cb) => {
        const fileExtension = path.extname(file.originalname);
        cb(null, file.fieldname + '-' + Date.now() + fileExtension); 
    }
});

const upload = multer({
    storage: storage,
    limits: { fileSize: 100000000 }
}).single("file");

module.exports = upload;

