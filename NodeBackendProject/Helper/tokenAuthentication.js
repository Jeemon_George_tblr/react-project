const jwt = require('jsonwebtoken');
const secretKey = "secretkey"; 

function authenticate(req, res, next) {
    const token = req.header('Authorization');
    if (!token) {
        return res.status(401).json({ message: 'Authentication failed.' });
    }
    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            return res.status(403).json({ message: 'Authentication failed. Invalid token.' });
        }
        req.user = decoded; 
        next(); 
    });
}

module.exports = authenticate;
