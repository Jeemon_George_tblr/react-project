var sequelize = require('../Config/dbconfig')
let Sequelize = require('sequelize');
let Role = require('../Model/Role')

//--------------  create User table  ------------------

const users = sequelize.define('users', {
    id: {
        primaryKey: true,
        autoIncrement: true,
        type: Sequelize.INTEGER,
    },
    first_name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    last_name: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    email: {
        type: Sequelize.STRING,
        allowNull: true,
        // unique: true,
    },
    phone_number: {
        type: Sequelize.STRING,
        allowNull: true,
        // unique: true,
    },
    password: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    password_salt: {
        type: Sequelize.STRING,
        allowNull: true,
    },
    status: {
        type: Sequelize.ENUM('active', 'inactive', 'trash'),
        defaultValue: 'active',
        allowNull: true,
    },
    role_id: {
        type: Sequelize.INTEGER,
        defaultValue: 1,
        allowNull: true,
        references: {
            model: 'roles',
            key: 'id'
        }
    },
    profile_image: {
        type: Sequelize.STRING
        // allowNull: true,        
    },

}, { freezeTableName: true });
users.sync().then(() => {
    console.log('Table created successfully');
}).catch((error) => {
    console.error('Error creating table:', error);
});
Role.hasMany(users, { foreignKey: 'role_id' });
users.belongsTo(Role, { foreignKey: 'role_id' });

module.exports = users;