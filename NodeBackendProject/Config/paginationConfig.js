
const paginate = (query, currentPage) =>{
    
    const itemsPerPage = 6; // ---------itemsPerPage here
    const offset = (currentPage - 1) * itemsPerPage;
    const limit = itemsPerPage;
    
    return {
        ...query,
        offset,
        limit,
    };
}

module.exports = paginate     
