const user = require('../Model/User');
var Role = require('../Model/Role');
var jwt = require("jsonwebtoken");
const HashPassword = require("../Helper/password").HashPassword
const verifyHashPassword = require("../Helper/password").verifyHashPassword
const nodemailer = require('nodemailer');
const mail = require('../Config/emailConfig');
const paginate = require('../Config/paginationConfig');
const { OAuth2Client } = require('google-auth-library');
const axios = require('axios')
// ----------------create Users -------------------------


// const addUser = async (req, res) => {
//     try {
//             const requredField = [
//             "first_name",
//             "email",
//             "phone_number",
//             "password",
//             // "profile_image",
//         ]
//         requredField.forEach(value => {
//             if (!req.body[value] || req.body[value].trim() == "") {
//                 return res.status(400).json({ error: "Required Field is Empty" });
//             }
//         });
//         let hashedPassword = HashPassword(req.body.password);
//         let createdUser = await user.create({
//             first_name: req.body.first_name,
//             last_name: req.body.last_name,
//             email: req.body.email,
//             phone_number: req.body.phone_number,
//             // profile_image:req.body.profile_image,  //img 
//             password: hashedPassword.hash,
//             password_salt: hashedPassword.salt
//         })


//         if (createdUser)
//             res.status(200).json(createdUser)
//         else
//             res.status(400).json({ Error: 'Error in insert new record' });
//     }
//     catch
//     (error) {
//         res.status(500).json({ error: error });
//     }
// }


// ------------mail included addUser-----

const addUser = async (req, res) => {
    try {
        const requiredFields = [
            "first_name",
            "email",
            "phone_number",
            "password",
        ];

        requiredFields.forEach(value => {
            if (!req.body[value] || req.body[value].trim() === "") {
                return res.status(400).json({ error: "Required Field is Empty" });
            }
        });

        
        const PhoneNumberExist = await user.findOne({
            where: {phone_number: req.body.phone_number }
        });
        if (PhoneNumberExist) {
           return res.status(400).json({error:"User already exists with this phone number"})
        }

        let hashedPassword = HashPassword(req.body.password);
        let createdUser = await user.create({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_number,
            password: hashedPassword.hash,
            password_salt: hashedPassword.salt,
        });

        if (createdUser) {
            // Send confirmation email
            mail(req.body.email)

            res.status(200).json(createdUser);
        } else {
            res.status(400).json({ Error: 'Error in inserting new record' });
        }
    } catch (error) {
        res.status(500).json({ error: error });
    }
};

//---------------------Google Signin----------------------------

const googleLogin = async (req, res) => {
    // try {

    // ...................gmail clientId decoded and check the token is valid-------------

    const { googleId } = req.body;
    const client = new OAuth2Client("293502210187-6p2fs40lg086qrfi0tpe0d2hg6d9tms0.apps.googleusercontent.com");

    const googleVerifyInfo = await client.verifyIdToken({
        idToken: req.body.tokenObj.id_token,
        audience: "293502210187-6p2fs40lg086qrfi0tpe0d2hg6d9tms0.apps.googleusercontent.com"
    });

    let decodedGoogleInfo = googleVerifyInfo.getPayload()
    console.log(decodedGoogleInfo, "response------------------")

    if (googleId == decodedGoogleInfo.sub) {
        const existingUser = await user.findOne({
            where: { email: decodedGoogleInfo.email }
        });

        let createFbUser;

        if (existingUser) {
            createFbUser = {
                id: existingUser.id,
                first_name: existingUser.first_name,
                last_name: existingUser.last_name,
                email: existingUser.email
            };
        } else {
            createFbUser = await user.create({
                first_name: decodedGoogleInfo.given_name,
                last_name: decodedGoogleInfo.family_name,
                email: decodedGoogleInfo.email,
            });
        }

        const token = jwt.sign(createFbUser, "secretkey", { expiresIn: 86400 });

        if (createFbUser) {
            return res.status(200).json({ success: true, createFbUser, token: token });
        } else {
            return res.status(401).json("Google login failed");
        }
    }
    else {
        return res.status(400).json("Unauthorized access")
    }

    // } catch (error) {
    //     res.status(500).json({ error: "Internal server Error" });
    // }
};

// .............................user login with facebook..........................

const facebookLogin = async (req, res) => {
    try {
        let { userId, accessToken } = req.body;
        let { data } = await axios.get(`https://graph.facebook.com/me?fields=id,name,email&access_token=${accessToken}`)
        console.log(data, "data--------------------")
        if (data.id != userId) {
            return res.status(400).json({ message: "Unauthorised access" });
        }

        const existingUser = await user.findOne({
            where: { email: data.email }
        });

        let createFbUser;

        if (existingUser) {
            createFbUser = {
                id: existingUser.id,
                first_name: existingUser.first_name,
                last_name: existingUser.last_name,
                email: existingUser.email
            };
        } else {
            let str = data.name.split(" ")
            console.log(str, "first-----------------")
            createFbUser = await user.create({
                first_name: str[0],
                last_name: str[1],
                email: data.email,
            });
        }

        const token = jwt.sign(createFbUser, "secretkey", { expiresIn: 86400 });

        if (createFbUser) {
            return res.status(200).json({ success: true, createFbUser, token: token });
        } else {
            return res.status(401).json("Facebook login failed");
        }
    } catch (error) {
        res.status(500).json({ error: "Internal server Error" });
    }
};






// -------------------img upload---------------
const imgUpload = async (req, res) => {
    try {
        console.log(req)
        const { filename } = req.file;
        const filepath = `uploads/${filename}`;
        if (req.file) {
            res.status(200).json({ message: 'File uploaded successfully.', filepath });
        } else {
            res.status(400).json({ error: "failed to upload image" });
        }
    } catch (error) {
        console.error('Error uploading file:', error);
        res.status(500).json({ error: 'Error uploading file.' });
    }
}

// ----------------- List ---------------------------


const getUsers = async (req, res) => {
    try {
        const currentPage = req.query.page || 1;
        const query = {
            include: [{
                model: Role,
                attributes: ['role_name'],
                required: true,
            }],
        };

        const paginatedQuery = paginate(query, currentPage);


        const usersList = await user.findAll(paginatedQuery);


        res.json(usersList);
    } catch (error) {
        res.status(500).json({ error: "Couldn't retrieve values" });
    }
};

// ----------------------List By ID-------------------------


const getUserById = async (req, res) => {
    try {
        const userId = req.params.id;
        const UserById = await user.findOne({
            where: { id: userId },
            include: [{
                model: Role,
                attributes: ['role_name'],
                required: true,
            }],
            attributes: {
                exclude: ['password', 'confirm_password', 'password_salt']
            }

        });

        if (UserById) {
            res.json(UserById);
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error retrieving user' });
    }
};


// ----------------------------Update User--------------------------


const updateUser = async (req, res) => {
    try {
        const userId = req.params.id;
        const updatedUser = await user.update({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_number,
            profile_image: req.body.profile_image ? req.body.profile_image : null
        }, {
            where: { id: userId },
        });

        if (updatedUser[0] === 1) {

            res.status(200).json({ message: 'User updated successfully' });
        } else {

            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error updating user' });
    }
};

// -------------------------DELETE A USER------------------


const deleteUser = async (req, res) => {
    console.log("request===========")
    try {
        const userId = req.params.id;
        const deleted = await user.destroy({
            where: { id: userId },
        });

        if (deleted) {

            res.status(200).json({ message: 'User deleted successfully' });
        } else {

            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error deleting user' });
    }
};




// ----------------LOgin------------------------

// const login = async (req, res) =>{
//     try {
//         let fields = ["email", "password"];
//         fields.map((item) => {
//             if (!req.body[`${item}`]) {
//                 let userdetails = { message: `${item}"is required"`, code: 400 }
//                 res.status(200).send({ userdetails });
//             }
//         })
//         const users = await user.findOne({
//             where: { email: req.body.email },
//             include: [{
//                 model: Role,
//                 attributes: ['role_name'],
//                 required: true,
//             }]
//         })
//         if (users) {
//             const secret = await verifyHashPassword(req.body.password, users.password_salt);
//             //tocken generation details
//             if (secret.hash == users.password) {
//                 let result = {
//                     id: users.id,
//                     firstname: users.first_name,
//                     lastname: users.last_name,
//                     email: users.email
//                 }
//                 let token = jwt.sign(result, "secretkey", { expiresIn: 86400 });
//                 //Display details
//                 let userdetails = {
//                     id: users.id,
//                     first_name: users.first_name,
//                     last_name: users.last_name,
//                     email: users.email,
//                     role_name: users.role.role_name,
//                     token: token
//                 }
//                 res.status(200).send({ userdetails });
//             }
//             else {
//                 let userdetails = { message: "authentication false", }
//                 res.send({ userdetails });
//             }
//         } else {
//             let userdetails = { message: "User doesnot exists", }
//             res.send({ userdetails });
//         }
//     } catch (e) {
//         return e;
//     }
// }

// --------------------------------

const login = async (req, res) => {
    try {

        let fields = ["email", "password"];
        fields.map((item) => {
            if (!req.body[`${item}`]) {
                let userdetails = { message: `${item} is required`, code: 400 }
                res.status(200).send({ userdetails });
            }
        });

        const users = await user.findOne({
            where: { email: req.body.email },
            include: [{
                model: Role,
                attributes: ['role_name'],
                required: true,
            }]
        });

        if (users) {
            const secret = await verifyHashPassword(req.body.password, users.password_salt);

            if (secret.hash == users.password) {
                let result = {
                    id: users.id,
                    firstname: users.first_name,
                    lastname: users.last_name,
                    email: users.email
                }
                let token = jwt.sign(result, "secretkey", { expiresIn: 86400 });

                let userdetails = {
                    id: users.id,
                    first_name: users.first_name,
                    last_name: users.last_name,
                    email: users.email,
                    role_name: users.role.role_name,
                    token: token
                }
                res.status(200).send({ userdetails });
            }
            else {
                let userdetails = { message: "Authentication failed", }
                res.status(401).send({ userdetails });
            }
        } else {
            let userdetails = { message: "User does not exist", }
            res.status(404).send({ userdetails });
        }
    }
    catch (e) {
        console.error(e);
        res.status(500).send({ message: "Internal server error" });
    }
}




module.exports = {
    addUser,
    getUsers,
    login,
    getUserById,
    updateUser,
    deleteUser,
    imgUpload,
    googleLogin,
    facebookLogin


};