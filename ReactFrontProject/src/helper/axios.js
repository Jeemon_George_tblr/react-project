import axios from "axios";
import config from "../config";
import Cookies from 'js-cookie';


const token = Cookies.get('authToken');
const customHeader = () => ({
  "Content-Type": "application/json",
  Accept: "application/json, text/plain, /",
  Authorization:token

});

export function api() {
  let opts = {
    baseURL: config.api.trim(),
    headers: customHeader(),
  };
  return axios.create(opts);
}
