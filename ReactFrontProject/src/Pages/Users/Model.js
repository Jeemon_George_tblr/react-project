import * as Service from './Service'

export const user = {
    state: {
        users: [],
        userDetails:[],
        addUser:[],
        // deleteUser:[],
        // updateUser:[],
        image:[]
      
     
    },
    reducers: {
        // Userlist fun
        onFetchUserSuccess: (state, data) => {
            return {
                ...state,
                users: data

            };
        },
        // UserDetailsfun
        onFetchUserDSuccess: (state, userdata) => {
            return {
                ...state,
                userDetails: userdata
            };
        },
        //postUser..
        onPostUserSuccess: (state, Postusers) => {
            return {
                ...state,
                addUser: Postusers
            };
        },
        // for deleting..
        // onDeleteUser: (state, deleteUser) => {
        //     console.log(deleteUser,"delete user reducer")
        //     return {
        //         ...state,
        //         deleteUser: deleteUser
        //     };
        // },

        // updating..
        
        onImageUpload: (state, update) => {
            return {
                ...state,
                updateUser: update
            };
        },
        // -----img update---

        onUpdateUser: (state, imgAdd) => {
            return {
                ...state,
                image: imgAdd
            };
        },
     
    },
    effects: () => ({
        // Userdelist..
        async getUsers(payload) {
            let res = await Service.getUsers(payload);
            this.onFetchUserSuccess(res); 
            return res;
        },
        //details..
        async getUsersDetails(payload) {
            let data = await Service.getUsersDetails(payload);
            this.onFetchUserDSuccess(data); 
            return data;
        },
        //post new user..
        async postUser(payload) {
            let data = await Service.postUser(payload);
            this.onPostUserSuccess(data); 
            return data;
        },
        // for deleting..
        async deleteUser(payload) {
            let data = await Service.deleteUser(payload);
            // this.onDeleteUser(data); 
            return data;
        },
       
          //    ----put user------------
       
        async putUser(payload) {
        let data = await Service.putUser(payload);
        // this.onUpdateUser(data); 
        return data;
    },
        // -----------img----

        async addimage(payload) {
        let data = await Service.addimage(payload);
        this.onImageUpload(data); 
        return data;
        },

    // -----------logOut----------------

    async logoutUser() {
    
        this.onLogout();
        return {}; 
    }


    }),





};