import React, { useEffect, useState } from 'react';
import '../../styles/style.css'
import SingleUser from './SingleUser';
import { Link } from "react-router-dom";
import PaginatedItems from '../../components/pagination/paginate'
import { useDispatch } from 'react-redux';




const List = () => {
  const dispatch = useDispatch()
  const [userList, setUserList] = useState([]);
  const [itemOffset, setItemOffset] = useState(1)
  // useEffect(() => {
  //   fetch(`https://reqres.in/api/users?page=${itemOffset}`)
  //     .then(response => response.json())
  //     .then(user => setPosts(user.data));

  // }, [itemOffset]);



  const fetchUsers = async() => {
    let res = await dispatch.user.getUsers(itemOffset)
    console.log(res, "res")
    setUserList(res)
}
useEffect(() => {
    fetchUsers()

}, [itemOffset])

  return (
    <div style={{padding: '50px'}}>
      <h1>User List</h1>
      <div className='ListUserProfile'>
        {userList.map(user => (
          <Link to={`/user/${user.id}`} className='items'>
            {console.log(user,"----------------------user before propagation--------------------")}
            <div onClick={e => e.stopPropagation()}>
           { console.log(user,"-----------------user after propagation-------------------")}
            <SingleUser user={user} userList={setUserList} itemOffset={itemOffset} updateOptions={true}/>
            </div>
          </Link>
        ))}
      </div>
      <PaginatedItems setItemOffset={setItemOffset} />
    </div>
  );
};

export default List;
