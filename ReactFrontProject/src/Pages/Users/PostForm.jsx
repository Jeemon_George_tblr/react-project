import React, { useState } from 'react';
import Button from "react-bootstrap/Button";
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from "yup";
import '../../styles/common.css';
import { useDispatch } from 'react-redux';
import Dropzone from 'react-dropzone';
import { useNavigate } from 'react-router-dom';


const PostForm = ({edited,user}) => {
  const navigate = useNavigate()
  const [file, setFile] = useState({})
  const dispatch = useDispatch()
  const initialValues = {
    id: user && user.id ? user.id : '',
    first_name: user && user.first_name ? user.first_name : '',
    last_name: user && user.last_name ? user.last_name : '',
    email: user && user.email ? user.email : '',
    phone_number: user && user.phone_number ? user.phone_number : '',
    profile_image:user && user.profile_image ? user.profile_image:''
  };

  const validationSchema = Yup.object({
    first_name: Yup.string().required("First Name is required"),
    last_name: Yup.string().required("Last Name is required"),
    email: Yup.string().email("Invalid email address").required("Email is required"),
    phone_number:  Yup.string(),
    profile_image: Yup.string(),

  });

 
// -----redux--------

  const fetchPostUser = async (values) => {
    let res;
    console.log({values})
    if(!edited){
       res = await dispatch.user.postUser(JSON.stringify(values))     
    }else{
      // console.log(file,"values in form =====");
      res= await dispatch.user.putUser(values)
      // alert("Are you sure!");
      // window.location.href = '/user';
  }
  console.log(res, "res")
  if(res && res.message) {
    navigate('/user')
  }
    }
    const UploadImage= async(image) =>{
      const formData = new FormData();
      formData.append("file", image);
      return await dispatch.user.addimage({file:formData});
    }


    const [selectedImage, setSelectedImage] = useState(null);

    const handleImageChange = (acceptedFiles) => {
      setSelectedImage(acceptedFiles[0]);
    };

// console.log(selectedImage?.path)
  return (

    <div className="d-flex justify-content-center align-items-center p-5 FormDesign">
      <div className='MyUserCard'>
        <div className="card p-5 ">
        <h2 className="signup">{!edited ? "ADD USER" : "EDIT USER"}</h2>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={fetchPostUser}
        >
{(formik) => (
          <Form className='formmain'>
            <div className="form-group">
              <Field
                type="text"
                name="first_name"
                className="form-control"
                placeholder="First Name"
              />
              <ErrorMessage
                name="first_name"
                component="div"
                className="error-message"
              />
            </div><br />

            <div className="form-group">
              <Field
                type="text"
                name="last_name"
                className="form-control"
                placeholder="Last Name"
              />
              <ErrorMessage
                name="last_name"
                component="div"
                className="error-message"
              />
            </div><br />

            <div className="form-group">
              <Field
                type="email"
                name="email"
                className="form-control"
                placeholder="Email"
              />
              <ErrorMessage
                name="email"
                component="div"
                className="error-message"
              />
            </div><br />
            <div className="form-group">
              <Field
                type="text"
                name="phone_number"
                className="form-control"
                placeholder=" Please enter Phone Number"
              />
              <ErrorMessage
                name="phone_number"
                component="div"
                className="error-message"
              />
            </div><br />
            <div className="form-group">
               
                {/* <Dropzone onDrop={handleImageChange} accept=".jpg, .jpeg, .png">
                  {({ getRootProps, getInputProps }) => (
                    <div className="dropzone" {...getRootProps()}>
                      <input {...getInputProps()} 
                      onChange={(e) => {

                        const selectedFile = e.target.files[0];
                        console.log(selectedFile,"values =====");
                        setFile(e.target.files[0])
                        
                    }}/>
                      {selectedImage ? (
                        <img
                          src={URL.createObjectURL(selectedImage)}
                          alt="Selected"
                          className="selected-image"
                        />
                      ) : (
                        <p>Drag &amp; drop an image here</p>
                      )}
                    </div>
                  )}
                </Dropzone> */}
                           <div className="inputBox">
                                <span >Image </span>
                                <input type="file" name="profile_image" onChange={async(e) => {
                                        const selectedFile = e.target.files[0];
                                        const result=await UploadImage(selectedFile);
                                        formik.setFieldValue('profile_image',result.filepath)
                                    }}/>
                            </div>


              </div>


            <Button type="submit">Submit</Button>
          </Form>
          )}
        </Formik>
      </div>
      </div>
    </div>
  );
};

export default PostForm;




