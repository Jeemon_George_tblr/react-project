import axios  from 'axios';
import config from '../../config';
import {  api } from "../../helper/axios";



export async function getUsers(payload) {
    return api()
    .get(`${config.routes.users}?page=${payload}`)
    .then((res) => res.data)
}
//  .........................
export async function getUsersDetails(payload) {
    return api()
    .get(`${config.routes.users}/${payload}`)
    .then((res) => res.data)
}

export async function postUser(payload) {
    return api()
    .post(config.routes.users, payload)
    .then((res) => res.data)
}

export async function deleteUser(payload) {
    return api()
    .delete(`${config.routes.users}/${payload}`)
    .then((res) => res.data)
   
}



export async function putUser(payload) {
   
    return api()
    .put(`${config.routes.users}/${payload.id}`,payload)
    .then((res) => res.data)
   
}

// export async function addimage(payload) {   
//     let response = await axios.post(`http://localhost:8080/api/users/img`, payload)
//     return response.data;
// }
export async function addimage(payload) {
   
    console.log("inside service", payload);
    return api()
    .post(config.routes.uploadimage,payload.file, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    })
    .then((res) => res.data)
} 

// ---------------LogOut--------------------

export async function logoutUser() {
    
    return { message: 'Logout successful' };
}
