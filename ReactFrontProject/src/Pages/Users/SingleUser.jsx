import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import Modal from 'react-modal';
import PostForm from './PostForm';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

function SingleUser({ user, itemOffset, userList, updateOptions }) {
  const dispatch = useDispatch();
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [edited, setEdited] = useState(false)

  const openModal = () => {
    setIsModalOpen(true);
    setEdited(true);
  };
  const closeModal = () => {
    setIsModalOpen(false);
  };

  // ----------------------------------delete ---------------------------
  const removeUser = async (e, id) => {

    e.preventDefault()
    const deletedUser = await dispatch.user.deleteUser(id);
    if (deletedUser) {
      toast.success('Success', {
        onClose: () => {
          if (deletedUser) {
            refreshpage();
          }
        }
      });
      // window.location.href = '/user';
    }
  };


  const refreshpage = async () => {
    let getUser = await dispatch.user.getUsers(itemOffset);
    userList(getUser)
  };
  // ---------------------------------------------------------------------------
  console.log(user.profile_image, user, "==========");

  return (


    <div className='body-items'>
      <Modal
        isOpen={isModalOpen}
        onRequestClose={closeModal}
        contentLabel="Edit Modal">
        <PostForm user={user} edited={edited} />
        <button onClick={closeModal}>Close</button>
      </Modal>
      <div className='data-container'>
        <div key={user.id} className='Singleuserlist'>
          <img src={`http://localhost:8080/${user.profile_image}`} alt="image" style={{ width: '50px', height: '50px' }} />

          <h3>{user.first_name}</h3>
          <h3>{user.last_name}</h3>
          <p>{user.email}</p>
          <p>{user.phone_number}</p>

          {
            updateOptions &&
            <div>
              <button onClick={(e) => removeUser(e, user.id)} style={{ backgroundColor: 'red', color: 'white' }} >Delete</button>
              <button onClick={openModal} style={{ backgroundColor: 'blue', color: 'white' }}>Update</button>
            </div>
          }

        </div>
        <ToastContainer
          position="top-right"
          autoClose={5000}
        />
      </div>
    </div>
  );
}

export default SingleUser;
