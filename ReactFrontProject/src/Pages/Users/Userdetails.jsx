import React from 'react'
import { useParams } from 'react-router';
import { useEffect, useState } from 'react'
import SingleUser from './SingleUser';
import { useDispatch } from 'react-redux';


const Userdetails = () => {
  const dispatch = useDispatch()
  const path = useParams();
  const [data, setData] = useState({});
 
 
  const fetchUserDetails = async () => {
    let data = await dispatch.user.getUsersDetails(path.id)
    setData(data);
    // console.log("aaaa", data);
  }
  useEffect(() => {
    fetchUserDetails()

  }, [path.id])

  return (
    <div>

      <SingleUser className="card" user={data} updateOptions={true}/>

    </div>
  )
}

export default Userdetails