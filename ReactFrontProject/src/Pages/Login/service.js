import {  api } from "../../helper/axios";
import config from '../../config/index';

export async function AddUser(payload) {
     return api()
    .post(config.routes.users,payload)
    .then((res) => res.data)
}

export async function Userlog(payload) {
    return api()
    .post(config.routes.login,payload)
    .then((res) => res.data)
}

export async function Guser(payload){
    return api()
    .post(config.routes.googleSignup,payload)
    .then((res) => res.data)

}

export async function Fuser(payload){
    return api()
    .post(config.routes.facebookSignup,payload)
    .then((res) => res.data)

}