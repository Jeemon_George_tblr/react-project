import React from 'react';
import '../Login/Login.css';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import Cookies from 'js-cookie';
function Login() {
  const dispatch = useDispatch();
  const initialValues = {
    email: '',
    password: '',
  };

  const validationSchema = Yup.object({
    email: Yup.string().email('Invalid email address').required('Email is required'),
    password: Yup.string().required('Password is required'),
  });

  const loginUser = async (values) => {
    const loginValues = await dispatch.User.Userlog(values);
    console.log(loginValues, "loginValues")
    Cookies.set('authToken', loginValues.userdetails.token);
    const authToken = Cookies.get('authToken');
    if (authToken) {
      window.location.href = '/home';
  } else {
      alert("Token is not present!");
  }
  };

  return (
    <div className="login-container">
      <div className="wrapper">
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={loginUser}
        >
          <Form>
            <h1>Login</h1>
            <div className="input-box">
              <Field type="email" name="email" placeholder="email" />
              < ErrorMessage name ="email" component="div" className=""/>
              <i className='bx bxs-user'></i>
              
            </div>
            <div className="input-box">
              <Field type="password" name="password" placeholder="password" />
              <ErrorMessage name="password" eomponent="div" className=""/>   
              <i className='bx bxs-lock-alt'></i>
            </div>
            <div className="remeber">
              <label>
                <Field type="checkbox" name="rememberMe" />
                Remember Me
              </label>
              {/* <a href="#">Forget password?</a> */}
            </div>
            <button type="submit" className="btn">Login</button>
            <div className="register">
              <p>Don't have an account ? </p>
              <a href="/SignUp"> Register</a>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
}

export default Login;