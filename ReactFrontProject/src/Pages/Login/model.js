import * as Service from './service'

export const User = {
    state : {
        users: [],
        login:[],
        guser:[],
        fuser:[]
    },
    reducers:{
        onPostUserSuccess: (state, users) => {
            return {
                ...state,
                users: users
            };
        },

        UserlogSuccess: (state, login) => {
            return {
                ...state,
                login: login
            };
        },

        GoogleUserlog:(state,guser)=>{
            return{
                ...state,
                guser:guser
            };
        },

        FacebookUserlog:(state,fuser)=>{
            return{
                ...state,
                fuser:fuser
            };
        }


    },
    effects:()=>({
        async AddUser(payload) {
            let data = await Service.AddUser(payload);
            this.onPostUserSuccess(data); 
            return data;
        },

        async Userlog(payload) {
            let data = await Service.Userlog(payload);
            this.UserlogSuccess(data); 
            return data;
        },

        async Guser(payload){
            let data = await Service.Guser(payload);
            this.GoogleUserlog(data);
            return data;

        },

        async Fuser(payload){
            let data = await Service.Fuser(payload);
            this.FacebookUserlog(data);
            return data;

        }
    })





}