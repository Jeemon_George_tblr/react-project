import React from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { jwtDecode } from "jwt-decode";
import Cookies from 'js-cookie';
import FacebookLogin from 'react-facebook-login';
import {GoogleLogin} from '@leecheuk/react-google-login';
import { gapi } from 'gapi-script';



function SignUp() {
  const clientId="293502210187-6p2fs40lg086qrfi0tpe0d2hg6d9tms0.apps.googleusercontent.com"
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const initialValues = {
    first_name: '',
    last_name: '',
    email: '',
    phone_number: '',
    password: '',
    confirmPassword: '',
  };

  const validationSchema = Yup.object({
    first_name: Yup.string().required('First Name is required'),
    last_name: Yup.string().required('Last Name is required'),
    email: Yup.string().email('Invalid email address').required('Email is required'),
    phone_number: Yup.string().required('Phone Number is required'),
    password: Yup.string().required('Password is required'),
    confirmPassword: Yup.string()

      .oneOf([Yup.ref('password'), null], 'Passwords must match')
    // .required('Confirm Password is required'),

  });

  const SignupUser = async (values) => {
    const res = await dispatch.User.AddUser(values);
    if (res.status === 'active') {
      navigate('/')
    }

  };

// ----------Facebook-----------

 

  const responseFacebook = async (response) => {
    try {
      console.log(response, "facebook login ");
      const facebookLogin = await dispatch.User.Fuser({ userId: response.userID, accessToken: response.accessToken });
      Cookies.set('authToken', facebookLogin.token);
      
      if (facebookLogin) {
        navigate('/home');
      } else {
        console.error('Facebook login failed or returned a falsy value.');
      }
    } catch (error) {
      console.error('An error occurred:', error);
    }
  }
  

// ------google-----------------

  const onSuccess = async(credentialResDecoded) => {
    const userGoogleLogin = await dispatch.User.Guser(credentialResDecoded);
    Cookies.set('authToken', userGoogleLogin.token);
    console.log(userGoogleLogin, "userLoginGoogle");
    if (userGoogleLogin) {
        navigate('/home');
    }
    function start() {
        gapi.client.init({
            clientId: clientId,
            scop: ""
        })
    }
    gapi.load('client:auth2', start)
};

const onFailure = (res) => {
};

  return (
    <div
      style={{
        backgroundImage: 'url("https://res.cloudinary.com/ddysmogx8/image/upload/v1690310033/GettyImages-1177482693_viqlun.webp")',
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        height: '100vh',
      }}
    >
      <link
        href='https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css'
        rel='stylesheet'
      />
      <div className="wrapper">
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={SignupUser}
        >
          <Form>
            <h1>SignUp</h1>
            <div className="input-box">
              <Field type="text" name="first_name" placeholder="First Name" />
              <ErrorMessage name="first_name" component="div" className="" />
              <i className='bx bxs-user'></i>
            </div>
            <div className="input-box">
              <Field type="text" name="last_name" placeholder="Last Name" />
              <ErrorMessage name="last_name" component="div" className="" />
              <i className='bx bxs-user'></i>
            </div>
            <div className="input-box">
              <Field type="email" name="email" placeholder="Email" />
              <ErrorMessage name="email" component="div" className="" />
              <i className='bx bxs-user'></i>
            </div>
            <div className="input-box">
              <Field type="text" name="phone_number" placeholder="Phone Number" />
              <ErrorMessage name="phone_number" component="div" className="" />
              <i className='bx bxs-user'></i>
            </div>
            <div className="input-box">
              <Field type="password" name="password" placeholder="Password" />
              <ErrorMessage name="Password" component="div" className="" />
              <i className='bx bxs-lock-alt'></i>
            </div>
            <div className="input-box">
              <Field type="password" name="confirmPassword" placeholder="Confirm Password" />
              <ErrorMessage name="confirmPassword" component="div" className="" />
              <i className='bx bxs-lock-alt'></i>
            </div>

            <input type="submit" className="btn" value="Sign Up" />


            {/* ...................google login...................... */}

            {/* <div>
            <GoogleLogin
              onSuccess={async (credentialRes) => {
                console.log("credentialRes.credential", credentialRes.credential)
                let credentialResDecoded = jwtDecode(credentialRes.credential);
                console.log("res", credentialResDecoded);
                const userGoogleLogin = await dispatch.User.Guser(credentialResDecoded);
                Cookies.set('authToken', userGoogleLogin.token);
                console.log(userGoogleLogin, "userLoginGoogle");
                if (userGoogleLogin) {
                    navigate('/home');
                }
            }}
              
              onError={() => {
                console.log('Login Failed');
              }}
            />
            </div> */}

            <GoogleLogin
              clientId={clientId}
              buttonText
              onSuccess={onSuccess}
              onFailure={onFailure}
              cookiePolicy={'single_host_origin'}
            />



            {/* ......................facebook login...................... */}

            <div>

              <FacebookLogin
                appId="362422622834717"
                autoLoad={false}
                fields="name,email,accessToken"
                // onClick={componentClicked}
                callback={responseFacebook} />

            </div>


            <div className="register">
              <p>Already have an account? </p>
              <a href="/"> Login</a>
            </div>
          </Form>
        </Formik>
      </div>
    </div>
  );
}

export default SignUp;
