
  const env = {
    development: {
      api:"http://localhost:8080/api",
    },
  };
  const all = {
    routes: {
      users: "users",
      login: 'users/login',
      uploadimage: 'users/img',
      googleSignup: 'users/Googlelogin',
      facebookSignup:'users/Facebooklogin',

 
      
    },
  };
  console.log("process.env.NODE_ENV",process.env.REACT_APP_ENV)
  const config={
    ...all,
    ...env[process.env.REACT_APP_ENV || "development"],
  }
  
  
  export default config;