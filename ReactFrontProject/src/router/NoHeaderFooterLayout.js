import React from 'react';

function NoHeaderFooterLayout({ children }) {
  return (
    <div>
      {children}
    </div>
  );
}

export default NoHeaderFooterLayout;
