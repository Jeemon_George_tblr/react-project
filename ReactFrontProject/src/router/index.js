import React from 'react';
import { Routes, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from '../store';
import Layout from './Layout';
import NoHeaderFooterLayout from './NoHeaderFooterLayout'; // Create this component
import Login from '../Pages/Login/Login';
import SignUp from '../Pages/Login/SignUp';
import BodyPage from '../components/bodypart';
import List from '../Pages/Users/List';
import UserDetails from '../Pages/Users/Userdetails';
import PostForm from '../Pages/Users/PostForm';
import Header from '../components/Header';
import Footer from '../components/Footer';

function Router() {
  return (
    <Provider store={store}>
      <Routes>
        {/* Routes without header and footer */}
        <Route path="/" exact element={<NoHeaderFooterLayout><Login /></NoHeaderFooterLayout>} />
        <Route path="/SignUp" element={<NoHeaderFooterLayout>< SignUp/></NoHeaderFooterLayout>} />

        {/* Routes with header and footer */}
        <Route path="/home" exact element={<Layout><BodyPage /></Layout>} />
        <Route path="/user" element={<Layout><List /></Layout>} />
        <Route path="/user/:id" element={<Layout><UserDetails /></Layout>} />
        <Route path="/SignUp" element={<Layout><PostForm /></Layout>} />
      </Routes>
    </Provider>
  );
}

export default Router;
