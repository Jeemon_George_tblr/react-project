import React from "react";
import Images from '../components/assets/images.jpeg'
import Images2 from'../components/assets/user1.png'
import Images3 from '../components/assets/download.png'
// import { Button} from 'react-bootstrap';
import Button from 'react-bootstrap/Button';

const Bodypage = () => {
  return (
    <div className="bdtxt">
      <h3 style={{ border: "25ch" }}>The User Registration of Startup</h3>
      <span>
        <h3>Framework</h3>
      </span>

      <div style={{ color: "rgb(129, 125, 125)" }}>
        <span>We have created a new User that will help designers, developers, companies, etc.</span>
      </div>

     
            <div className="crd">
              <div className="col-sm-4">
                <div className="card">
                  <img
                  
                    src={Images} 
                    style={{ height: "150px", width: "274px" }}
                    alt="Card image cap"/>
                  
                  <div className="card-block">
                    <h4 className="card-title">Manager</h4>
                    <p className="card-text">
                    Buy Latest Running, Training & Sports-style Collection online. Free & Fast Shipping
                    </p>
                    <Button type="button" className="btn btn-primary blue-btn">
                      See More
                    </Button>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card">
                  <img
                    className="card-img-top img-fluid"
                    src={Images2} 
                    style={{ height: "150px", width: "274px" }}
                    alt="Card image cap"/>
                  <div className="card-block">
                    <h4 className="card-title">CEO</h4>
                    <p className="card-text">
                    Buy Latest Running, Training & Sports-style Collection online. Free & Fast Shipping
                    </p>
                    <Button type="button" className="btn btn-primary">
                      See More
                    </Button>
                  </div>
                </div>
              </div>
              <div className="col-sm-4">
                <div className="card">
                  <img
                    className="card-img-top img-fluid"
                    src={Images3} 
                    style={{ height: "150px", width: "274px" }}
                    alt="Card image cap"
                  />
                  <div className="card-block">
                    <h4 className="card-title">Employees</h4>
                    <p className="card-text">Buy Latest Running, Training & Sports-style Collection online. Free & Fast Shipping.</p>
                    <Button type="button" className="btn btn-primary">
                      See More
                    </Button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        
  );
};

export default Bodypage;
