import React from "react";
import '../styles/common.css'
import '../index.css'
import { Link, useNavigate } from "react-router-dom";
import { useCookies } from 'react-cookie'



// const Header = () => {

  const Header = () => {
    const [cookies, setCookie, removeCookie] = useCookies(['authToken']);
    const navigate = useNavigate(); // Initialize useNavigate
  
    const logout = () => {
      removeCookie('authToken');
      navigate("/"); // Navigate to the root path ("/") after logout
    };



  return (
    <nav className=" p-3 bg-dark w-100 d-flex  align-items-center justify-content-between">
      <div >
        <a  href="#">
          <h5 className="ml-10">Company</h5>
        </a>
      </div>
      <ul className="main-header p-0 mb-0">
        <li>
        <Link to="/">Login</Link>
        </li>
        <li>
          <Link to="/user">User</Link>
        </li>
        <li>
          <Link to="/SignUp">Add User</Link>
        </li>
        <li>
          <Link to="/" >
          <span onClick={logout} className="clickable">Logout</span>
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
              className="bi bi-box-arrow-in-right" viewBox="0 0 16 16">
              <path fillRule="evenodd"
                d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0v-2z" />
              <path fillRule="evenodd"
                d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708l3-3z" />
            </svg>
            </Link>
        </li>
      </ul>
    </nav>
  );
  // }
};

export default Header;

