import React from 'react';
import ReactPaginate from 'react-paginate';
import '../../styles/style.css'





export default function PaginatedItems({ setItemOffset}) {

  const handlePageClick = (event) => {
    setItemOffset(event.selected + 1);
  };

  return (
   
    <>
      <ReactPaginate
        className='pagination'
        breakLabel="..."
        nextLabel="next >"
        onPageChange={handlePageClick}
        pageCount={2}
        previousLabel="< previous"
        pageRangeDisplayed={10}
        marginPagesDisplayed={2}
      />
    </>
   
  );
}