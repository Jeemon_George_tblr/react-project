import React from "react";
import '../styles/common.css'
import '../index.css'

const Footer = () => {
  return (
 
      <footer className="ftr">
        <div className="footer-items">
          <h6>Home</h6>
          <h6>About</h6>
          <h6>Contact</h6>
          <h6>Sign-in</h6>
        </div>
        <h6 className="ftr-1">© 2023 Company: Jeemon George</h6>
      </footer>
 
  );
};

export default Footer;
