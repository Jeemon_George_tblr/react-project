import React from 'react';
import './App.css';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Router from './router';
import { BrowserRouter } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import Bodypage from './components/bodypart';
import { Routes, Route } from 'react-router-dom';
import List from './Pages/Users/List';
import Userdetails  from './Pages/Users/Userdetails';
import 'bootstrap/dist/css/bootstrap.min.css';
import PostForm from './Pages/Users/PostForm';
import Login from './Pages/Login/Login'
import { GoogleOAuthProvider } from '@react-oauth/google';



function App() {
  return (
    
    <div className="App">
      <GoogleOAuthProvider clientId="293502210187-6p2fs40lg086qrfi0tpe0d2hg6d9tms0.apps.googleusercontent.com">
        <Router />
        <ToastContainer />
      </GoogleOAuthProvider>
 
    </div>
  );
}

export default App;
