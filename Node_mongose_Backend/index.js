const express = require('express');
let db = require('./Config/dbconfig');



//-------MiddleWare------------------//


const app = express();
// app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));



const RoleRouter = require('./Routes/roleRoute.js');
app.use('/api/roles', RoleRouter)

const UserRouter = require('./Routes/userRoute.js');
app.use('/api/users', UserRouter)



app.get('/',(req,res)=>{
    res.send('Welcom to Mongoose ')
});

app.listen(3000,()=>{
    console.log("Server is running Successfully in 3000!");
});

