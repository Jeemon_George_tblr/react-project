const userController = require('../Controller/userControler');
const router = require('express').Router();

router.post('/addUser', userController.addUser);
router.get('/getUser', userController.getUser);
router.put('/updateUser/:id', userController.updateUSer);
router.delete('/deleteUser/:id', userController.deleteUser);
router.delete('/deleteUser/:id', userController.deleteUser);
router.post('/login', userController.login);
router.post('/logout', userController.logout);


module.exports = router