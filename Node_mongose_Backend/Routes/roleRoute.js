const roleController = require('../Controller/roleController');
const router = require('express').Router();

router.post('/addroll', roleController.addRoles);
router.get('/getroles', roleController.getRoles);


module.exports = router