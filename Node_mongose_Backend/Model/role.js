const mongoose = require('mongoose');
var Dbconfig = require('../Config/dbconfig');

// const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
  role_name: {
    type: String,
    required: true   
}
});

const Role = mongoose.model('roles', roleSchema);

module.exports = Role;
