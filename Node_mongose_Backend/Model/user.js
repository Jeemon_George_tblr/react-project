let mongoose = require('mongoose');
let db = require('../Config/dbconfig');


const Schema = mongoose.Schema;

const userSchema = new mongoose.Schema({
    first_name: {
        type: String,
        required: true,
    },
    last_name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    phone_number: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    password_salt: {
        type: String,
    },
    status: {
        type: String,
        enum: ['active', 'inactive', 'trash'],
        default: 'active',
        required: true,
    },
    role_id: {
        type: Schema.Types.ObjectId,
        ref: 'roles',
        required: true,
    },
}, { timestamps: true }); 

const user = mongoose.model('users', userSchema);

module.exports = user;
