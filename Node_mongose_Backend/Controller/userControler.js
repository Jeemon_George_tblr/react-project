const user = require('../Model/user');
var jwt = require('jsonwebtoken');
const hashPassword = require("../Helper/password").hashPassword
const verifyHashPassword = require('../Helper/password').verifyHashPassword
// const role = require('../Model/role')
 
// --------------------- create Users ------------------------

const addUser = async (req, res) => {
    try {
        const requiredFields = [
            "first_name",
            "email",
            "phone_number",
            "password"
        ];

        for (const field of requiredFields) {
            if (!req.body[field] || req.body[field].trim() === "") {
                return res.status(400).json({ error: "Required Field is Empty" });
            }
        }

        const hashedPassword = await hashPassword(req.body.password);
        console.log("hashedPassword===",hashedPassword);
        const newUser = new user({
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_number,
            password: hashedPassword.hashedPassword,
            password_salt: hashedPassword.salt,
            role_id: req.body.role_id
        });

        const createdUser = await newUser.save();

        if (createdUser) {
            res.status(200).json(createdUser);
        } else {
            res.status(400).json({ Error: 'Error in inserting new record' });
        }
    } catch (error) {
        res.status(500).json({ error: error.message });
    }
};

// ------------------List User----BY ID And ALL USer USing params----------------------------


const getUser = async (req, res) => {
    try {
        console.log(req.query.id);
        let UserDetails
        if (req.query.id) {
            UserDetails = await user.findById({ _id: req.query.id })
        } else {
            UserDetails = await user.find()
        }
        if (!UserDetails || UserDetails === "" || UserDetails === null) throw err


        if (UserDetails) {
            res.status(200).json({ data: UserDetails })
        } else {
            res.status(400).error;
        }
    } catch (err) {
        console.log(err, "err")
        res.status(400).json({ error: "some error occured", err: err })
    }
}

// ----------------update User--------------------

const updateUSer = async (req, res) => {
    try {
        const userId = req.params.id;

        const updateData = {
            first_name: req.body.first_name,
            last_name: req.body.last_name,
            email: req.body.email,
            phone_number: req.body.phone_number,
        };
        const updateUSer = await user.findByIdAndUpdate(userId, updateData);
        if (updateUSer) {
            res.status(200).json({ message: 'User updated successfully' });
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error updating user' });
    }
}
// --------------DELETE USER-------------------

const User = require('../Model/user'); 

const deleteUser = async (req, res) => {
    try {
        const userId = req.params.id;

        const deletedUser = await User.findByIdAndRemove(userId);

        if (deletedUser) {
            res.status(200).json({ message: 'User deleted successfully' });
        } else {
            res.status(404).json({ error: 'User not found' });
        }
    } catch (error) {
        res.status(500).json({ error: 'Error deleting user' });
    }
};


// ---------------------Login--------------------


const login = async (req, res) => {
    // try {
        const fields = ["email", "password"];
        for (const item of fields) {
            if (!req.body[item]) {
                const userdetails = { message: `${item} is required`, code: 400 };
                return res.status(400).send({ userdetails });
            }
        }

        const user = await User.findOne({ email: req.body.email }).populate('role_id');
console.log(user,"======")
        if (user) {
            console.log('heeeeeeeeee',user.password_salt);
            console.log('hiiiiiiiiiiiiii',req.body.password);

            const secret = await verifyHashPassword(req.body.password, user.password_salt);
            console.log('secret===',secret);
            console.log("hash===",secret,"=====");
            console.log("pass===",user.password,"=====");

            if (secret) {
                console.log('huuuuuuuuu');

                const result = {
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email
                };

                const token = jwt.sign(result, "secretkey", { expiresIn: 86400 });

                const userdetails = {
                    id: user.id,
                    first_name: user.first_name,
                    last_name: user.last_name,
                    email: user.email,
                    role_name: user.role_id.role_name,
                    token: token
                };
                res.status(200).send({ userdetails });
            } else {
                const userdetails = { message: "Authentication failed" };
                res.send({ userdetails });
            }
        } else {
            const userdetails = { message: "User does not exist" };
            res.send({ userdetails });
        }
    // } catch (error) {
    //     res.status(500).json({ error: 'An error occurred while processing the request' });
    // }



};
// --------------------------LogOut-------------------------

const logout = (req, res) => {
   

    res.clearCookie('authToken'); 

    res.status(200).send({ message: 'Logout successful' });
    res.redirect('/');
};





module.exports = {
    addUser,
    getUser,//included get by ID 
    updateUSer,
    deleteUser,
    login,
    logout

}