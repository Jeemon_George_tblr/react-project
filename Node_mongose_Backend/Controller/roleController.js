const role = require('../Model/role');

// ------------------add Role -------------------//



const addRoles = async (req, res) => {
  try {
    if (!req.body.role_name || req.body.role_name.trim() === "") {
      return res.status(400).json({ "error": "role name is required" });
    } else {
      const createdRole = await role.create({ role_name: req.body.role_name });
      if (createdRole) {
        res.status(200).json(createdRole);
      } else {
        res.status(400).json({ Error: 'Error in insert new record' });
      }
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

//--------------LIST All -----------------------------------------//

const getRoles = async (req, res) => {
  try {
      const roleslist = await role.find();
      res.json(roleslist);
  } catch (error) {
      res.status(500).json({ error: "con't Retrieving values" });
  }
}






// ----------- Exports ------------------

module.exports = {
    addRoles,
    getRoles,

};






