const bcrypt = require('bcrypt');

const SALT_ROUNDS = 10;

const hashPassword = async (password) => {
    const salt = await bcrypt.genSalt(SALT_ROUNDS);
    const hashedPassword = await bcrypt.hash(password, salt);
    return {hashedPassword,salt};
};

const verifyHashPassword = async (password, salt) => {
    const hashedPassword = await bcrypt.hash(password, salt);
    return {hashedPassword,salt};
};

module.exports = { hashPassword, verifyHashPassword };
